Archive Block Extension for Mecha
=================================

Release Notes
-------------

### 2.3.4

 - Make block example becomes copy-paste friendly.

### 2.3.3

 - Added more options: `deep`, `path`, `time`.
